//
// https://www.geeksforgeeks.org/horners-method-polynomial-evaluation/
// https://www.obliquity.com/computer/fortran/procedure.html
//
#include<stdio.h>
#include<stdlib.h>

//  write our own integer exponentiation in order to avoid the intrinsic float `pow`
double intpow(const double b, const int n) {
 if ( n == 0 ) return 1.0;
 else return b*intpow(b,n-1);
} 

//  naive computation of the polynomial from the coefficients
//   the complexity includes the multiplications that occur in `intpow`
double f(const double x, const double* coeffs, const unsigned int deg) {
 double answer=0.0;
 for ( int i = 0; i<deg; i++ ) {
  answer = answer + coeffs[i]*intpow(x,deg-i-1);
 }
 return answer;
}

// the following is from geeksforgeeks
// what about ? https://en.wikipedia.org/wiki/Horner%27s_method#C_implementation
double fh(const double x, const double* coeffs, const unsigned int deg) {
    double answer = coeffs[0];
    for ( int i = 1; i < deg ; i++ ) {
        answer = answer * x + coeffs[i];
    }
    return answer;
}

//  a polynomial has been hard-coded ... we choose the argument to f(x)
int main(int argc, char *argv[]) {
 // f(x) = -0.87*x^3 + 4.2*x^2 -1.2*x + 9.3
 double coeffs[] = {-0.87, 4.2, -1.2, 9.3};
 unsigned int deg = sizeof(coeffs)/sizeof(coeffs[0]);
 char *ptr;
 double x = strtod( argv[1], &ptr);
 double dummy;

 //  loop many times for measureability
 for ( int i=0; i<10000000; i++ ) {
   dummy = fh(x, coeffs, deg);
 }

 printf("\n degree of %d\n",  deg-1 );
 printf("  fh(x) = %f\n", fh(x, coeffs, deg) );
 printf("   f(x) = %f\n",  f(x, coeffs, deg) );
 printf(" manual = %f\n\n",
     coeffs[0]*x*x*x + coeffs[1]*x*x + coeffs[2]*x + coeffs[3] );

 return 0;
}

