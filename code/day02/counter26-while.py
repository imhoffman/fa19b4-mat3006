
def counter( c, s, accum=0 ):
    if not s:          # perhaps c was at the end of the previous string
        return accum
    while True:        # s must have something in it, so search it for c
        i = s.find(c)
        if i == -1:
            return accum   # this is the only way out of the while loop
        accum = accum + 1  # c must have been found; increment and loop again
        s = s[i+1:]


herp = 'o'
derp = 'Advent of Code'
print( " there are %d %s's in %s\n" % ( counter( herp, derp ), herp, derp ) )

