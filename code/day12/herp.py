#!/usr/bin/env python3

class derp:
  def __init__(self, base, exponent):
      self.base = base
      self.exponent = exponent

  def adder ( self, arg ):
      b = self.base
      e = self.exponent
      return b**arg + arg**e

  def subtracter ( self, arg ):
      b = self.base
      e = self.exponent
      return b**arg - arg**e


##
##  main program
##

my_derp = derp( 3.0, 2.0 )
your_derp = derp( 2.0, 3.0 )

print( my_derp.subtracter(1.5), your_derp.subtracter(1.5) )

