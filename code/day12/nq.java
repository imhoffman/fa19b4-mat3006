import java.util.Random;

public class nq {
 public static void main(String[] args) {

  double xmin, xmax, ymin, ymax, answer;
  int choice=0, total, trials = 1;
  Random r = new Random(System.currentTimeMillis());
  double[] Stats = new double[2];

  xmin=0.; xmax=0.; ymin=0.; ymax=0.; total=0;

  if ( args.length == 6 || args.length == 7 ) {
   choice = Integer.parseInt(args[0]);
   xmin = Double.parseDouble(args[1]);
   xmax = Double.parseDouble(args[2]);
   ymin = Double.parseDouble(args[3]);
   ymax = Double.parseDouble(args[4]);
   total = Integer.parseInt(args[5]);
   if ( args.length == 7 ) {
    trials = Integer.parseInt(args[6]);
   }
  } else {
   banner.out();
   System.exit(1);
  }
  func f = new func(choice);
  banner.out(f);

  if ( trials == 1 ) {
   answer = quadrature.monte(f,xmin,xmax,ymin,ymax,total,r);
   System.out.format("\n    Monte Carlo: %+22.16f\n",f.display(answer));
  } else {
   Stats = quadrature.monte(f,xmin,xmax,ymin,ymax,total,trials,r);
   System.out.format("\n    Monte Carlo: %+22.16f",f.display(Stats[0]));
   System.out.format(" +/- %18.16f\n",f.display(Stats[1]));
  }

  answer = quadrature.Simpson(f,xmin,xmax,total);
  System.out.format("\n Simpson's Rule: %+22.16f\n",f.display(answer));

  answer = quadrature.Boole(f,xmin,xmax,total);
  System.out.format("\n   Boole's Rule: %+22.16f\n\n",f.display(answer));
 }
}

class func {
 private static int i;
 func(int choice) {
  i = choice;
 }

 static double f(double x) {
  switch (i) {
   case 1: return Math.exp(-x*x);
   case 2: return Math.sqrt(1.0 - x*x);
   case 3: return x*x*x;
   default: return x;
  }
 }

 static String label() {
  switch (i) {
   case 1: return "  square of exp(-x*x) ";
   case 2: return "four times sqrt(1-x^2)";
   case 3: return "          x^3         ";
   default: return "       problem        ";
  }
 }

 static double display(double x) {
  switch (i) {
   case 1: return x*x;
   case 2: return 4.0*x;
   default: return x;
  }
 }
}

class quadrature {
 static double monte(func g, double xMin, double xMax, double yMin, double yMax, int nPoints, Random r) {
  int i, counter, inside, outside;
  double x, y;

  counter = 0; inside = 0; outside = 0;
  for ( i=0; i<nPoints; i++ ) {
   x = xMin + (xMax-xMin)*r.nextDouble();
   y = yMin + (yMax-yMin)*r.nextDouble();
   if ( 0.0 <= y && y <= g.f(x) ) {
        counter++; inside++;
   } else if ( g.f(x) <= y && y < 0.0 ) {
        counter--; inside++;
   } else {
        outside++;
   }
  }
  if ( inside + outside != nPoints ) {
   System.out.println("\ndarts don't add up!\n\n");
   return Math.sqrt(-1.0);
  }
  return (double)counter/(double)nPoints*(xMax-xMin)*(yMax-yMin);
 }

 static double[] monte(func g, double xMin, double xMax, double yMin, double yMax, int nPoints, int nTrials, Random r) {
  int i, j, counter, inside, outside;
  double x, y, avg, stdev;
  double[] area = new double[nTrials];
  double[] stats = new double[2];

  for ( j = 0; j<nTrials; j++ ) {
   counter = 0; inside = 0; outside = 0;
   for ( i=0; i<nPoints; i++ ) {
    x = xMin + (xMax-xMin)*r.nextDouble();
    y = yMin + (yMax-yMin)*r.nextDouble();
    if ( 0.0 <= y && y <= g.f(x) ) {
        counter++; inside++;
    } else if ( g.f(x) <= y && y < 0.0 ) {
        counter--; inside++;
    } else {
        outside++;
    }
   }
   if ( inside + outside != nPoints ) {
    System.out.println("\ndarts don't add up!\n\n");
    return new double[]{Math.sqrt(-1.0),Math.sqrt(-1.0)};
   }
   area[j] = (double)counter/(double)nPoints*(xMax-xMin)*(yMax-yMin);
  }
  avg = 0.0;
  for ( j=0; j<nTrials; j++ ) {
   avg = avg + area[j];
  }
  avg =(double) avg/nTrials;
  stdev = 0.0;
  for ( j=0; j<nTrials; j++ ) {
   stdev = stdev + (area[j] - avg)*(area[j] - avg);
  }
  stats[1] =(double) Math.sqrt( stdev/nTrials );
  stats[0] = avg;
  return stats;
 }

 static double Simpson(func g, double xMin, double xMax, int nPoints) {
  double deltaX = (xMax-xMin)/(double)nPoints;
  double area = 0.0, x;
  int i;
  for ( i=0; i<nPoints; i++) {
   x = xMin+((double)i+0.5)*deltaX;
   area = area + deltaX/8.0*( g.f(x-deltaX/2.0) + 3.0*g.f((3.0*x-deltaX/2.0)/3.0) + 3.0*g.f((3.0*x+deltaX/2.0)/3.0) + g.f(x+deltaX/2.0) );
  }
  return area;
 }

 static double Boole(func g, double xMin, double xMax, int nPoints) {
  double deltaX = (xMax-xMin)/(double)nPoints;
  double area = 0.0, x, h = deltaX/4.0;
  int i;
  for ( i=0; i<nPoints; i++) {
   x = xMin+((double)i+0.5)*deltaX;
   area = area + 2.0*h/45.0*( 7.0*g.f(x-2.0*h) + 32.0*g.f(x-h) + 12.0*g.f(x) + 32.0*g.f(x+h) + 7.0*g.f(x+2.0*h) );
  }
  return area;
 }
}

class banner {
 private static int RESET = 0, BRIGHT = 1, DIM = 2, UNDERLINE = 3, BLINK = 4, REVERSE = 7, HIDDEN = 8;
 private static int BLACK = 0, RED = 1, GREEN = 2, YELLOW = 3, BLUE = 4, MAGENTA= 5, CYAN = 6, WHITE = 7;

 static void out() {
  textColor(BRIGHT,RED,BLACK);
  System.out.println("");
  System.out.println(" ###############################################################");
   System.out.format(" #                    ");
  textColor(BRIGHT,MAGENTA,BLACK);
                          System.out.format("numerical quadrature");
  textColor(BRIGHT,RED,BLACK);
                                               System.out.format("                     #\n");
   System.out.format(" # ");
   textColor(BRIGHT,GREEN,BLACK);
       System.out.format("  choices:   1: exp(-x*x)     2: sqrt(1-x^2)     3: x^3    ");
   textColor(BRIGHT,RED,BLACK);
                                         System.out.format(" #\n");
   System.out.format(" # ");
   textColor(DIM,RED,BLACK);
       System.out.format("usage: java nq choice xMin xMax yMin yMax nPoints [nTrials]");
  textColor(RESET,WHITE,BLACK);
   textColor(BRIGHT,RED,BLACK);
                                                                   System.out.format(" #\n");
  System.out.println(" ###############################################################");
  System.out.println("");
  textColor(RESET,WHITE,BLACK);
 }

 static void out(func c) {
  textColor(BRIGHT,RED,BLACK);
  System.out.println("");
  System.out.println(" ###############################################################");
   System.out.format(" #                    ");
  textColor(BRIGHT,MAGENTA,BLACK);
                         System.out.format("numerical quadrature");
  textColor(BRIGHT,RED,BLACK);
                                              System.out.format("                     #\n");
   System.out.format(" #                   ");
   textColor(BRIGHT,GREEN,BLACK);
                              System.out.format("%s",c.label());
   textColor(RESET,WHITE,BLACK);
   textColor(BRIGHT,RED,BLACK);
                                         System.out.format("                    #\n");
   System.out.format(" # ");
   textColor(DIM,RED,BLACK);
      System.out.format("usage: java nq choice xMin xMax yMin yMax nPoints [nTrials]");
   textColor(RESET,WHITE,BLACK);
   textColor(BRIGHT,RED,BLACK);
                                                               System.out.format(" #\n");
  System.out.println(" ###############################################################");
  System.out.println("");
  textColor(RESET,WHITE,BLACK);
 }

 static void textColor(int attr, int fg, int bg) {
  System.out.format("%c[%d;%d;%dm", 0x1B, attr, fg + 30, bg + 40);
 }
}
