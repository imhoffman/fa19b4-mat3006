
from math import sqrt

def is_triangle( A, B, C ):
    D = [ A, B, C ]
    E = max( D )
    D.remove( E )
    F = sum( D )
    if ( F > E ):
        return True
    else:
        return False


class triangle:
    def __init__(self, A, B, C):
        self.A = A
        self.B = B
        self.C = C
        assert is_triangle( self.A, self.B, self.C )
        return

    def perimeter( self ):
        return self.A + self.B + self.C

    def heron( self ):
        A = self.A
        B = self.B
        C = self.C
        S = self.perimeter() / 2.0
        return sqrt( S*(S-A)*(S-B)*(S-C) )

    def quattro( self ):
        A = self.A
        B = self.B
        return A * B / 2.0


my_triangle = triangle( 3, 4, 6 )

print( "\n the perimeter is %d\n" % my_triangle.perimeter() )

print( "\n the heron area: %f;  the quattro area: %f\n\n"
        % ( my_triangle.heron(), my_triangle.quattro() ) )



