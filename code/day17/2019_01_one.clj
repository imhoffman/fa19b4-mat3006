;;   2019 Advent Day 01
;;
;;  subprograms
;;
(defn fuel [mass]
  (- (/ (- mass (mod mass 3)) 3) 2))

(defn fuel-java-floor [mass]
  (int (- (Math/floor (/ mass 3)) 2)))


;; recursive function that traverses the input list itself
;;  called initially with accum equal to zero
;;  https://clojure.org/guides/learn/flow#_defn_and_recur
(defn total-fuel [list-of-masses accum]
  (if (empty? list-of-masses)
    accum
    (recur (rest list-of-masses) (+ accum (fuel (first list-of-masses))))))

;;
;;  main program
;;
;;   file I/O
(def input
  (with-open [f (clojure.java.io/reader "puzzle.txt")]
    (reduce conj () (line-seq f))))      ;; lists add to the beginning
;    (reduce conj [] (line-seq f))))      ;; vectors add to the end

(println "Read" (count input) "lines.")

;;  comprehend the list of string inputs as parsed integers
(def input-as-ints
  (for [line input]
    (Integer/parseInt line)))
;; end of file I/O and parsing

;;
;;  puzzle solution
;;

;;   one way to do it
;;    comprehend the list of masses as a list of fuels, then
;;    add up all the members of the list
;;    https://clojuredocs.org/clojure.core/apply
(println
  " part one: the fuel needed for the module masses is"
  (apply + (for [mass-of-module input-as-ints] (fuel mass-of-module))))


;;   another way to do it
;;    use the function defined among the subprograms
(println
  " part one: the fuel needed for the module masses is"
  (total-fuel input-as-ints 0))

