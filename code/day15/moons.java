import java.io.*;
import java.lang.String;
import java.util.HashMap;
import java.util.Random;

public class moons {
 final static int MAX_LINES = 5;
 public static void main ( String[] args ) {
   Random r = new Random(System.currentTimeMillis());
   int[] pos = new int[3];
   int[] vel = new int[3];

   HashMap<Phase, Integer> hashed_history = new HashMap<Phase, Integer>(MAX_LINES);
   //hashed_history.put( new Phase( new int[] { 3, 1, 14 }, new int[] { 8, 4, 7} ), MAX_LINES+1 );
   for ( int j=0; j < MAX_LINES; j++ ) {
     for ( int i=0; i < 3; i++ ) {
       pos[i] = r.nextInt(16);
       vel[i] = r.nextInt(16);
     }
     hashed_history.put( new Phase( pos, vel ), j );
   }
   System.out.printf( "\n HashMap has %d entries\n", hashed_history.size() );
   System.out.println( hashed_history );


   int j_match = hashed_history.get(
       new Phase( new int[] { 3, 1, 14 }, new int[] { 8, 4, 7} )
       );
   System.out.printf( "\n Found a match at iteration %d\n", j_match );

 return;
 }

}

class Phase {
  private int[] x = new int[3];
  private int[] v = new int[3];
  public Phase( int[] x_, int[] v_ ) {
    this.x = x_; this.v = v_;
  }

  void contents ( ) {
    System.out.printf( " this object contains x: %d %d %d, v: %d %d %d\n",
       	this.x[0], this.x[1], this.x[2], this.v[0], this.v[1], this.v[2] );
    return;
  }

  @Override
  public int hashCode() {
    return this.x.hashCode() + this.v.hashCode();
  }

}
