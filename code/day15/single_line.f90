!!
!! typedefs and parameters
!!
 module types
  !use iso_fortran_env
  implicit none
  !integer, parameter            :: iw = int64
  integer, parameter            :: max_line_length = 16384
 end module types

!!
!! subroutines and functions
!!
 module subs
  use types
  implicit none
  contains

  !! read external file and count characters
  subroutine reader ( fp, number_of_characters, file_content )
   implicit none
   integer, intent(in)  :: fp
   integer, intent(out) :: number_of_characters
   character (len=*)    :: file_content
   integer :: i

   read(fp,'(A)',err=100,end=200) file_content
   number_of_characters = len_trim( file_content )

   goto 200
   100 write(6,*) ' error reading file'
   200 continue
   return
  end subroutine reader
 end module subs

!!
!! main program
!!
 program main
  use types
  use subs
  implicit none
  character (len=max_line_length), allocatable :: temp
  character(:)                   , allocatable :: input
  integer :: fileunit=10, line_length

  !!  file I/O
  ! read external file into temporary array
  allocate( temp )
  open(fileunit,file="puzzle.txt")
  call reader ( fileunit, line_length, temp )
  close(fileunit)

  ! populate input and free reading buffer
  allocate( character(line_length) :: input )
  input = trim( temp )
  deallocate( temp )

  write(6,'(A,I0,A)') ' Read one line of ', line_length, ' characters'
  !! end of file I/O

  !!
  !!  puzzle solution
  !!




  deallocate ( input )
  stop
 end program main

