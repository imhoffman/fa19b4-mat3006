!!
!! typedefs and parameters
!!
 module types
  !use iso_fortran_env
  implicit none
  !integer, parameter            :: iw = int64
  integer, parameter            :: max_lines = 16384
  integer, parameter            :: max_line_length = 101
 end module types

!!
!! subroutines and functions
!!
 module subs
  use types
  implicit none
  contains

  !! read external file and count lines
  subroutine reader ( fp, nlines, longest_line, file_content )
   implicit none
   integer,            intent(in)  :: fp
   integer,            intent(out) :: nlines, longest_line
   character (len=*), dimension(*) :: file_content
   integer :: i

   longest_line = 0
   nlines = 0
   do i = 1, max_lines
    read(fp,*,err=100,end=200) file_content(i)
    nlines = nlines + 1
    if ( len_trim( file_content(i) ) .gt. longest_line ) then
            longest_line = len_trim( file_content(i) )
    endif
   end do
   goto 200
   100 write(6,*) ' error reading file'
   200 continue
   return
  end subroutine reader
 end module subs

!!
!! main program
!!
 program main
  use types
  use subs
  implicit none
  character (len=max_line_length), dimension(:), allocatable :: temp
  character(:)                   , dimension(:), allocatable :: input
  integer :: fileunit=10, number_of_lines, longest_line_length
  integer :: i

  !!  file I/O
  ! read external file into temporary array
  allocate( temp(max_lines) )
  open(fileunit,file="puzzle.txt")
  call reader ( fileunit, number_of_lines, longest_line_length, temp )
  close(fileunit)

  ! populate input and free reading buffer
  allocate( character(longest_line_length) :: input(number_of_lines) )
  do i = 1, number_of_lines
   input(i) = trim( temp(i) )
  end do
  deallocate( temp )

  write(6,'(A,I0,A,I0,A)') &
          ' Read ', number_of_lines, &
          ' lines; the longest line is ', longest_line_length, ' long'
  !! end of file I/O

  !!
  !!  puzzle solution
  !!




  deallocate ( input )
  stop
 end program main

