#include<stdio.h>
#include<stdlib.h>

int main(void) {
 int i;
 double x[] = { 3.2, -4.5, 0.001, -78.78 };
 double* p;

 printf("\n");

 p = &x[0];
 for ( i = 0; i < 1048576; i++ ) {
  printf(" *(p+%04d) = %18.11e at %p\n", i, *(p+i), (p+i));
 }

 return 0;
}

