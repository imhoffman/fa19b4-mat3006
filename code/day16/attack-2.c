#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<stdbool.h>

int main(void) {

 char *x =(char *) malloc( 7 * sizeof(char) );
 char *y =(char *) malloc( 7 * sizeof(char) );
 strcpy( x, "psswrd" );

 bool *correct =(bool *) malloc( sizeof(bool) );
 *correct = false;

 printf( "\n  &x[0] = %p,  &correct = %p\n\n", x, correct );

 for ( int i = 0; i < 10; i++ ) {
   printf( "\n  x[%1d] = %c;   &x[%1d] = %p\n", i, x[i], i, &x[0]+i );
 }

 //  note that the memory slotsused by fgets for y will extend
 //  beyond the y malloc and into the space that is allocated
 //  to `correct`, thus changing the value of `correct`
 printf( "\n Please enter the password: " );
 fgets( y, 80, stdin );
 //memset( &y[0]+6, '\0', sizeof(char) );
 //  uncommenting the preceding line will permit any password that
 //  agrees with the first six characters to pass muster; usually,
 //  hashes would be compared, as we saw in Intro

 for ( int i = 0; i < 40; i++ ) {
   printf( "\n  y[%1d] = %c;   &y[%1d] = %p\n", i, y[i], i, &y[0]+i );
 }

 if ( ! strcmp( x, y ) ) {
   *correct = true;
 } else {
   printf("\n Wrong password.\n\n");
 }

 //  typing a string longer than ~33 characters for fgets will overwirte
 //  `correct` in memory
 if ( *correct ) { printf( "\n   You're in !!\n\n" ); }

 free( x );
 free( y );
 free( correct );

 return 0;
}

