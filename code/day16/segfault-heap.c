#include<stdio.h>
#include<stdlib.h>

int main(void) {
 int i;
 double* p;

 printf("\n");

 p =(double *) malloc( 5 * sizeof(double) );
 for ( i = 0; i < 1048576; i++ ) {
  printf(" *(p+%04d) = %18.11e at %p\n", i, *(p+i), (p+i));
 }

 free( p );

 return 0;
}

