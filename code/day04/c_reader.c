#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#define LENSTR        80
#define MAX_LINES     16384

//
//  subroutine for determining length of file and longest single line
//  the file read is held in a large temporary array that is free'ed
//  in main once an appropriately-sized array is allocated and populated
//
void reader ( FILE* f, int* n, int* maxlen, char lines[][LENSTR] ) {
  char buffer[LENSTR] = {'\0'};
  char *qnull;
  size_t maxtemp;

  *maxlen=0;
  *n = 0;
  while ( fgets(buffer, LENSTR, f) != NULL ) {
    strncpy( lines[*n], buffer, LENSTR );
    qnull = strchr( buffer, '\0' );  // no need to memset buffer to all \0 every loop
    maxtemp = qnull - &buffer[0];    // arithmetic among memory addresses
    if ( (int)maxtemp > *maxlen ) { *maxlen =(int) maxtemp; }
    (*n)++;
  }
  return;
}


//
// main program
//
int main( int argc, char *argv[] ) {
 FILE* fp;
 int n, longest_line;
 //char (*temp)[LENSTR] = malloc( MAX_LINES * sizeof( *temp ) );   // would also work
 char (*temp)[LENSTR] = malloc( MAX_LINES * sizeof( char ) * LENSTR );

 fp = fopen("input.txt","r");
 reader( fp, &n, &longest_line, temp );  // n and longest_line are known upon return
 fclose(fp);        // done with file...only read the file once

 printf( "\n read %d lines from the input file\n", n );
 printf( "\n the longest line is %d chars long\n", longest_line );

 char keeper[n][longest_line+1];   // declare an array of appropriate length and width

 for( int i=0; i < n; i++ ) {
  strncpy( keeper[i], temp[i], longest_line+1 );
 }
 free( temp );  // keeper now has the data...free the big temp array from the file read

 printf( "\n the 556th entry: %s\n", keeper[555] );

 return 0;
}

