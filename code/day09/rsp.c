// https://stackoverflow.com/questions/24039024/how-to-set-a-gdb-watchpoint-to-a-value-stored-in-register
// https://www.cs.umd.edu/~srhuang/teaching/cmsc212/gdb-tutorial-handout.pdf

int incrementer ( int *n ) {
  *n = *n + 1;
  return *n + 5;
}


int decrementer ( int *n ) {
  *n = *n - 1;
  return *n - 5;
}


void swapper ( int *n, int *m ) {
 int temp;
 temp = *m;
 *m = *n;
 *n = *m;
 return;
}


int main (void ) {
  int j, k;

  j = 3;

  k = incrementer( &j );

  j = decrementer( &k );

  swapper( &j, &k );

  return j + k;
}
