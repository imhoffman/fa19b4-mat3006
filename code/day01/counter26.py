
def counter( c, s, accum=0 ):
    if not s:
        return accum
    i = s.find(c)
    if i == -1:
        return accum
    else:
        return counter( c, s[i+1:], accum+1 )

herp = 'o'
derp = 'Advent of Code'
print( " there are %d %s's in %s\n" % ( counter( herp, derp ), herp, derp ) )

